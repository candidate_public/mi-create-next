#!/usr/bin/env node

const { execSync } = require("child_process");
const path = require("path");
const fs = require("fs");

if (process.argv.length < 3) {
  console.log("You have to provide a name to your app.");
  console.log("For example :");
  console.log("    npx mi-create-next my-app");
  process.exit(1);
}

const projectName = process.argv[2];
const typescript = process.argv[3] === "--ts";
const currentPath = process.cwd();
const projectPath = path.join(currentPath, projectName);
const GIT_REPO_JAVASCRIPT =
  "https://gitlab.com/candidate_public/mi-boilerplate-nextjs";
const GIT_REPO_TYPESCRIPT =
  "https://gitlab.com/candidate_public/mi-boilerplate-nextjs-typescript";

const createDirectory = async () => {
  try {
    fs.mkdirSync(projectPath);
  } catch (err) {
    if (err.code === "EEXIST") {
      console.log(
        `The file ${projectName} already exist in the current directory, please give it another name or delete the existing directory.`
      );
    } else {
      console.log(err);
    }
    process.exit(1);
  }
};

const installBoilerplateLocally = async () => {
  try {
    console.log("Downloading files...");
    execSync(
      `git clone --depth 1 ${
        typescript ? GIT_REPO_TYPESCRIPT : GIT_REPO_JAVASCRIPT
      } ${projectPath}`
    );
    process.chdir(projectPath);
    console.log("Removing useless files");
    execSync("npx rimraf ./.git");
    console.log(`Installation is done.`);
    console.log(`cd ${projectName}`);
    console.log(`npm install`);
    console.log(`npm run dev`);
  } catch (err) {
    console.log(err);
  }
};

const main = () => {
  try {
    createDirectory();
    installBoilerplateLocally();
  } catch (err) {
    console.log(err);
  }
};

main();
