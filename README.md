# mi-create-next

A simple CLI to install [Next JS Boilerplate](https://gitlab.com/Esa_MI/mi-boilerplate-nextjs) to your machine with no build configuration. Tool built for productivity.

Works on Windows and macOS. Not yet tested on Linux.

## Quick Overview

**We recommend you to use the latest LTS version of [Node JS](https://nodejs.org/en/) or v18.12.1 higher**

If you've previously installed `mi-create-next` globally via `npm i -g mi-create-next`, we recommend you to uninstall the package using `npm un -g mi-create-next` to ensure that npx always uses the latest version

```bash
npx mi-create-next <project_name>
cd <project_name>
npm install
npm run dev
```

If you want to use TypeScript, add `--ts` flag at the end of the command

```bash
npx mi-create-next <project-name> --ts
```

Then open [http://localhost:3000/](http://localhost:3000/) to see your app.

### Get Started Immediately

You don't need to setup any tools like redux, folder structures, api, etc.

Create a project, and you’re good to go.

### Tech Debt

- No `yarn` or `pnpm` support, only `npx` for current version.
